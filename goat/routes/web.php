<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/matches', function () {
    return view('matches');
});

Route::get('/index', function () {
    return view('index')->name('index');
});

Route::get('/contact', function () {
    return view('contact');
});

Route::get('/about', function () {
    return view('about');
});

Route::get('/team', function () {
    return view('team');
});

Route::get('/test', function () {
    return view('test');
});

Route::get('/news', function () {
    return view('news');
});

Route::get('/mufc', function () {
    return view('mufc');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/api', 'HomeController@api');



Route::get('/positions','PositionController@create')->name('allpositions');

// Route::get('/players','PlayerController@create')->name('allplayers');

Route::get('/store_players','PlayerController@add_player')->name('storeplayers');

Route::get('/store_teams','TeamController@add_team')->name('storeteams');

Route::get('/store_position','PositionController@add_position')->name('storepositions');

Route::get('/getformation','FormationController@create')->name('formation');

Route::get('/player_formation_delete/{id}','FormationController@destroy');

Route::post('/post_formation','FormationController@store');

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');


Route::get('/pp','PlayerController@test');

// Route::get('/newsscource', 'ApiController@newsapi');
// Route::post('/source_id', 'ApiController@newsapi');

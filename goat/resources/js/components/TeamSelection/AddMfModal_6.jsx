import React, { Component } from 'react';
import { Modal, Button } from 'react-bootstrap';


export class AddMfModal_6 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            myArray: [],
            midfielders: []
        }
    }
    send(player_id, team_name, player_firstname, player_secondname, value) {
        //    this.state.arr.push(event);
        var joined = this.state.myArray.concat(event);
        this.setState({ myArray: joined })
        this.props.onHide();
        this.props.hideimage_6([team_name, player_firstname, player_secondname, value, player_id]);
    }

    componentDidMount(e) {
        this._isMounted = true;
        fetch("/positions")
            .then(result => result.json())
            .then((results) => {
                if (this._isMounted) {
                    this.setState({
                        isLoaded: true,
                        midfielders: results.midfielders
                    })
                }
            },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    })
                })
    }

    componentWillUnmount() {
        this._isMounted = false;
    }


    render() {
        const { midfielders } = this.state;

        return (
            <Modal
                {...this.props}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        Add Midfielder
              </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="midfieldermodal">
                        {midfielders.map((midfielder) => (
                            <ul key={midfielder.player_id} value={midfielder.position_id}>
                                <img src={"../images/" + midfielder.team_name + ".png"} style={{ height: 30, width: 30 }} />
                                {midfielder.player_firstname} {midfielder.player_secondname} ({midfielder.team_shortname})     {midfielder.value}M
                                <p align='right'>
                                    <Button variant='warning' onClick={this.send.bind(this, midfielder.player_id, midfielder.team_name, midfielder.player_firstname, midfielder.player_secondname, midfielder.value)} >choose</Button>
                                </p>
                            </ul>
                        ))}
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant='danger' onClick={this.props.onHide}>Close</Button>
                </Modal.Footer>
            </Modal>
        )
    }

}
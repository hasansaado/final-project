import React, { Component } from 'react';
import { Modal, Button } from 'react-bootstrap';


export class AddFwModal_2 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            myArray: [],
            forwards: []
        }
    }
    send(player_id, team_name, player_firstname, player_secondname, value) {
        //    this.state.arr.push(event);
        var joined = this.state.myArray.concat(event);
        this.setState({ myArray: joined })
        this.props.onHide();
        this.props.hideimage_2([team_name, player_firstname, player_secondname, value, player_id]);    }

    componentDidMount(e) {
        this._isMounted = true;
        fetch("/positions")
            .then(result => result.json())
            .then((results) => {
                if (this._isMounted) {
                    this.setState({
                        isLoaded: true,
                        forwards: results.forwards
                    })
                }
            },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    })
                })
    }

    componentWillUnmount() {
        this._isMounted = false;
    }
    joined

    render() {
        const { forwards } = this.state;

        return (
            <Modal
                {...this.props}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        Add Forward
              </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="forwardmodal">
                        {forwards.map((forward) => (
                            <ul key={forward.player_id} value={forward.position_id}>
                                <img src={"../images/" + forward.team_name + ".png"} style={{ height: 30, width: 30 }} />
                                {forward.player_firstname} {forward.player_secondname} ({forward.team_shortname})     {forward.value}M
                                <p align='right'>
                                    <Button variant='warning' onClick={this.send.bind(this, forward.player_id, forward.team_name, forward.player_firstname, forward.player_secondname, forward.value)} >choose</Button>
                                </p>
                            </ul>
                        ))}
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant='danger' onClick={this.props.onHide}>Close</Button>
                </Modal.Footer>
            </Modal>
        )
    }

}